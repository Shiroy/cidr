# cidr

`cidr` is a typescript library that helps to manipulate CIDR blocks.

[Click here to access the documentation](https://shiroy.gitlab.io/cidr/)

## Usage

```typescript
import { CIDRBlock } from "@eryldor/cidr";

const cidr = CIDRBlock.fromString("10.0.0.0/16");

//Print "10.0.0.0"
console.log(cidr.networkAddress);
//Print "16"
console.log(cidr.networkPrefix);
//Print "10.0.255.255"
console.log(cidr.broadcastAddress)

//Let's divide it into 2 subnets
const subnets = cidr.split(2);
//Print "10.0.0.0/17"
console.log(subnets[0].toString());
//Print "10.0.128.0/17"
console.log(subnets[1].toString());

//Enumerate all the IPs except the network and broadcast address.
for(ip of cidr.ipAddress()) {
    console.log(ip);
}
```